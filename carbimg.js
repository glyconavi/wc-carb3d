import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-location/iron-location.js';

class GlyconaviCarbImg extends PolymerElement {
  static get template() {
    return html`
<style>

</style>
  <div>
    <iron-location path="{{path}}" hash="{{hash}}" query="{{query}}" dwell-time="{{dwellTime}}"></iron-location>
    <img src="http://carb3d.glyconavi.org/img/pdb/cc/2d/png/PDBCC-[[retrieveId(query)]].png" alt="[[retrieveId(query)]]" width="" title="PDB Chemical Component">
  </div>
`;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
  retrieveId(query) {
    console.log(query);
    var params = query.split("=");

    return params[1];
  }
}

customElements.define('wc-carbimg', GlyconaviCarbImg);
